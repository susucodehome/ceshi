# python+selenium实现自动化登录
1. 这里是列表文本为了保证隐私，在代码运行前，163邮箱登录需要将用户名密码修改为用户本人的账号密码：
    login_name = "用户名"
    login_passwd = "密码"
2. 这里是列表文本京东登录需要将用户名密码修改为用户本人的账号密码：
    id = "*********" 
    passwd = "******"
- 注意：对于京东的自动化登录滑块验证阶段只是定位到了具体元素，暂未通过轨迹实现成功，还需进一步的研究。
 **其它详细介绍请参考本人CSDN博客：** [https://blog.csdn.net/Sudahoney/article/details/103226939](https://blog.csdn.net/Sudahoney/article/details/103226939)
